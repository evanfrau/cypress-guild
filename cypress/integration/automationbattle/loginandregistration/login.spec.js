///<reference types="Cypress"/>

describe('login', () => {

    before(() => {
        cy.fixture('testdata.json').as('data')
    })

    beforeEach(() => {
        cy.visit("/")
        cy.get(".ico-login").click()
    })

    it('filling the data and click to login', () => {
        const password = Cypress.env("password")
        cy.checkEnvPassword(password);

        cy.get('@data').then((data) => {
            const { email } = data.users[0]

            cy.get(".returning-wrapper > .title > strong")
                .should("be.visible")

            cy.get("#Email")
                .type(email)
                .should("have.value", email)

            cy.get("#Password")
                .type(password,  { log: false })
                .should(pass => {
                    if (pass.val() !== password) {
                      throw new Error('Different value of typed password')
                    }
                })

            cy.get("form > .buttons > .button-1").click()

            cy.get(".header-links > ul > :nth-child(1) > .account")
                .should("have.text", email)
        })
    })

    // This test should fail for demonstrating purpose
    // This test use softAssert library to assert all the values and prevent from being blocked by the first failing assertations
    it('Email and Password still filled in when remember button is checked', () => {
        const email = "cypress@gmail.com"
        const password = Cypress.env('password')
        const rememberIsChecked = true

        cy.checkEnvPassword(password);

        cy.login(rememberIsChecked)
        cy.logout()
        cy.get(".ico-login").click()

        cy.get("#Email").then(($value) => {
            cy.softAssert($value.text(), email, `should be equal to ${email}`)
        })
        cy.get("#Password").then(($value) => {
            cy.softAssert($value.text(), password, `should be equal to ${password}`)
        })
        cy.get("#RememberMe").then(($value) => {
            cy.softAssert($value.checked, rememberIsChecked, "should be checked")
        })
        cy.softAssertAll()

    })

    context("testing multiple data", () => {
        const data = require("../../../fixtures/testdata.json");

        data.users.forEach((user) => {
            const { email } = user;
            it(`login with  ${email}`, () => {
                cy.login(false, email);
            });
        });
    });
})
