///<reference types="Cypress"/>

describe('registration', () => {

    beforeEach(() => {
        cy.visit('http://demowebshop.tricentis.com/')
        cy.get(".ico-register").click()
    })

    it('fill all the mandatory data and complete the registration', () => {
        const firstName = createRandomStrings()
        const lastName = createRandomStrings()
        const email = `${firstName}@gmail.com`
        const password = createRandomStrings()

        cy.get("#gender-male")
            .click()
            .should("be.checked")

        cy.get("#FirstName")
            .type(firstName)
            .should("have.value", firstName)

        cy.get("#LastName")
            .type(lastName)
            .should("have.value", lastName)

        cy.get("#Email")
            .type(email)
            .should("have.value", email)

        cy.get("#Password")
            .type(password)
            .should("have.value", password)

        cy.get("#ConfirmPassword")
            .type(password)
            .should("have.value", password)

        cy.get("#register-button").click()

        cy.get(".result").should("be.visible")
        cy.get('.header-links > ul > li > .account').should("have.text", email)
        cy.get(".ico-logout").should("be.visible")
    })

    it('Try to complete the registration process when you do NOT fill in all mandatory fields', () => {
        const firstName = createRandomStrings()
        const lastName = createRandomStrings()
        const email = `${firstName}@gmail.com`
        const password = createRandomStrings()

        cy.get("#gender-male")
            .click()
            .should("be.checked")

        cy.get("#FirstName")
            .type(firstName)
            .should("have.value", firstName)

        cy.get("#LastName")
            .type(lastName)
            .should("have.value", lastName)

        cy.get("#Email")
            .type(email)
            .should("have.value", email)

        cy.get("#Password")
            .type(password)
            .should("have.value", password)

        cy.get("#register-button").click()

        cy.get('#ConfirmPassword').nextUntil('.field-validation-error > span').should("be.visible")

    })

    it('Check Display error message when non-matching ‘password’ and ‘confirm password’ fields', () => {
        const firstName = createRandomStrings()
        const lastName = createRandomStrings()
        const email = `${firstName}@gmail.com`
        const password = createRandomStrings()

        cy.get("#gender-male")
            .click()
            .should("be.checked")

        cy.get("#FirstName")
            .type(firstName)
            .should("have.value", firstName)

        cy.get("#LastName")
            .type(lastName)
            .should("have.value", lastName)

        cy.get("#Email")
            .type(email)
            .should("have.value", email)

        cy.get("#Password")
            .type(password)
            .should("have.value", password)

        cy.get("#register-button").click()
        cy.get('.field-validation-error > span').should('be.visible').screenshot()
        cy.get('.center-2').screenshot() // screenshot of the section to get full context
    })
})

function createRandomStrings() {
    return Math.random().toString(36).substring(3);
}