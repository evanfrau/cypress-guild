///<reference types="Cypress"/>

describe('Search And Filtering', () => {
    beforeEach(() => {
        cy.visit("/")
        cy.login(true)
    })
    it('Check you can sort based on the name of products, from A-Z and verify that the sorting is correct', () => {
        //We initialise 2 Arrays.
        let initialArray = []
        let sortedArray = []
        cy.get('.top-menu > :nth-child(1) > a').click()
        cy.get('#products-orderby').select('Name: A to Z')
        //We take the number of products and iterate over each of them and add the name to 2 arrays.
        cy.get('.product-title').its('length').then(($el) => {
            for (var k = 0; k < $el; ++k) {
                cy.get('.product-title > a')
                    .eq(k)
                    .then(($element) => {
                        let value = $element.text()
                        //.push is a JS function to add an item to the end of an array
                        initialArray.push(value)
                        sortedArray.push(value)
                    })
            }
            //Here we use the .sort() function that sorts alphabetically the array 
            sortedArray.sort()
            //Here we assert if the initialArray is deepEqual with the sorted array that we made.
            assert.deepEqual(initialArray, sortedArray);
        })

    })

    it('Check that you can switch between grid and list views for products', () => {
        //We go to any category and then select the "List" view
        cy.get('.top-menu > :nth-child(1) > a').click()
        cy.get('#products-viewmode').select('List')

        //We check if the products are in list view based on the CSS class ".product-list" being changed depending on the view
        cy.get('.product-list').should('be.visible')

        //We select the "Grid" view
        cy.get('#products-viewmode').select('Grid')
        //We check if the products are in list view based on the CSS class ".product-grid" being changed depending on the view
        cy.get('.product-grid').should('be.visible')

    })

    it('Search for a specific product, based on a key-word', () => {
        //We use the "computer as the key-word"
        let searchKeyword = 'computer'
        //We type the key-word in the search bar
        cy.get('#small-searchterms').type(searchKeyword)
        cy.get('form > .button-1').click()
        //We take each element that is shown after the search with the "key-word"
        cy.get('.product-title').its('length').then(($el) => {
            for (let l = 0; l < $el; l++) {
                cy.get('.product-title > a')
                    .eq(l)
                    //For each element we check if the key-word is present in the name of the element (Again we have to set to LowerCase to avoid case sensitive issues)
                    .then(($element) => {
                        expect($element.text().toLowerCase()).to.contain(searchKeyword)
                    })
            }

        })
    })

    it('Check that you can filter products based on the category, and verify that the filtering is correct', () => {
        let searchKeyword = 'phone'
        //We are clicking on 'Electronics'
        cy.get('.list > :nth-child(3) > a').click()
        //We are  clicking on 'Cell phones'
        cy.get('.active > .sublist > :nth-child(2) > a').click()
        //We get each item shown on screen that has been filtered and check if they have "phone"
        cy.get('.product-title').its('length').then(($el) => {
            for (let l = 0; l < $el; l++) {
                cy.get('.product-title > a')
                    .eq(l)
                    .then(($element) => {
                        //We put the element to LowerCase to avoid any case sensitivity issues for the assertion
                        expect($element.text().toLowerCase()).to.contain(searchKeyword)
                    })
            }
        })

    })
    
    it('Search a product with the advanced search, using a specific category and price range', () => {
        //We click on a specific category
        cy.get('.list > :nth-child(3) > a').click()
        cy.get('.active > .sublist > :nth-child(1) > a').click()
        //We take a specific price range
        cy.get('.price-range-selector > :nth-child(1) > a').click()
        //We loop over each element that is in that specific category and price range
        cy.get('.product-title').its('length').then(($el) => {
            for (let l = 0; l < $el; l++) {
                cy.get('.actual-price')
                    .eq(l)
                    .then(($element) => {
                        //On each element we check if the price value is in the correct "price range"
                        const finalprice = parseInt($element.text());
                        expect(finalprice).to.be.below(500)
                    })
            }
        })

    })

    it('Check you can sort based on the price of products, from highest to lowest and verify that the sorting is correct', () => {
        //We initialize 2 arrays
        let initialArray = []
        let sortedArray = []
        cy.get('.top-menu > :nth-child(4) > a').click()
        //We select the price from highest to lowest
        cy.get('#products-orderby').select('Price: High to Low')
        //We loop over each element in order from highest to lowest
        cy.get('.product-title').its('length').then(($el) => {
            for (let l = 0; l < $el; l++) {
                //We take the price from the element
                cy.get('.actual-price')
                    .eq(l)
                    .then(($element) => {
                        //For each element we add the price to 2 arrays
                        let value = $element.text()
                        //Again we use the .push() function of JS
                        initialArray.push(value)
                        sortedArray.push(value)
                    })
            }
            //Here we are sorting the second Array again from highest to lowest (As you can see I am using compareNumbers() function I made below)
            sortedArray.sort(compareNumbers);
            //We assert that both arrays are deepEqual to check if the sorting was correct or not
            assert.deepEqual(initialArray, sortedArray);
        })
    })

    it('If the price is equal, the products should be listed from A-Z', () => {
        //We declare a tempvalue that will be used later
        let tempvalue = 0;
        //Again we initialise 2 arrays that we will compare
        let firstArray = []
        let sortedArray = []
        //We initialise a counter that will be used later
        let counter = 1;
        cy.get('.top-menu > :nth-child(1) > a').click()
        //Here I sort the products from High to low
        cy.get('#products-orderby').select('Price: High to Low')
        //I loop over each product again
        cy.get('.product-title').its('length').then(($el) => {
            for (let l = 0; l < $el; l++) {
                //We get the actual price of each item
                cy.get('.actual-price')
                    .eq(l)
                    .then(($element) => {
                        let value = $element.text()
                        //Here we check if the price of the element is the same as the last element "tempvalue"
                        if (value == tempvalue) {
                            //Here We add the current element to both arrays
                            cy.get(`:nth-child(${counter}) > .product-item > .details > .product-title > a`).then(($ele) => {
                                firstArray.push($ele.text())
                                sortedArray.push($ele.text())
                            })
                            //Here we add the last element to both arrays
                            cy.get(`:nth-child(${counter - 1}) > .product-item > .details > .product-title > a`).then(($ele) => {
                                firstArray.push($ele.text())
                                sortedArray.push($ele.text())
                            })
                            //Here we assert that both arrays are deepEqual which would mean that both elements are sorted correctly from A-Z
                            assert.deepEqual(firstArray, sortedArray);

                        }
                        //Here we add 1 to the counter each time we loop over an element
                        counter = counter + 1;
                        //We take the name of the current element and set it in the tempvalue to assert it on next loop index
                        tempvalue = $element.text()
                    })
            }
        })
    })

})

function compareNumbers(a, b) {
    return a - b;
}
