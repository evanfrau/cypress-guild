///<reference types="Cypress"/>

describe('Shopping basket', () => {

    beforeEach(() => {
        cy.visit("/")
        cy.login(true)
        cy.fixture('testdata.json').as('data')
    })

    it("Adding an item to the basket and increasing the counter of 1", () => {
        let counter = 0;
        cy.intercept({
            method: "POST",
            url: "http://demowebshop.tricentis.com/addproducttocart/details/72/1"
        }).as("addOneProduct")

        cy.get('.top-menu > :nth-child(2) > [href="/computers"]').click()

        cy.get(".sub-category-grid > :nth-child(1)").click()

        cy.get(":nth-child(1) > .product-item > .details > .add-info > .buttons > .button-2").click()
        cy.get('.cart-qty').then(($element) => {
            cy.log('initial: ' + $element.text());
            counter = extractNumberFromShoppingCart($element.text())

            cy.get("#add-to-cart-button-72").click()
        })
        cy.wait("@addOneProduct")
        cy.get('.cart-qty').then(($finalElement) => {
            cy.log('final: ' + $finalElement.text());
            const finalNumber = extractNumberFromShoppingCart($finalElement.text())

            expect(finalNumber).to.be.equal(counter + 1)
        })
    })

    it("Add multiple same products to your shopping basket and check increase counter accordingly", () => {
        let counter = 3
        let nameProduct = "Fiction"

        cy.visit("/books")
        cy.addProduct(2)
        cy.addProduct(2)
        cy.addProduct(2)

        cy.get('.cart-qty').click()

        cy.get('.cart-item-row').eq(0).find('.qty-input').should("have.value", counter)
        cy.get('.cart-item-row').eq(0).find('.product-name').should("have.text", nameProduct)
    })

    it("Add multiple different products to your shopping basket and check if the counter increases with the same amount of products added", () => {
        let counter = 3

        cy.visit("/books")
        cy.addProduct(0)
        cy.addProduct(2)
        cy.addProduct(4)
        cy.get('.cart-qty').click()

        cy.get('.cart-item-row').eq(0).find('.qty-input').should("have.value", 1)
        cy.get('.cart-item-row').eq(1).find('.qty-input').should("have.value", 1)
        cy.get('.cart-item-row').eq(2).find('.qty-input').should("have.value", 1)
        cy.get('.cart-qty').then(($element) => {
            const number = extractNumberFromShoppingCart($element.text())
            expect(number).to.be.equal(counter)
        })
    })

    it("Check that total price of your shopping cart is correct. Check that you can completely empty your shopping basket when there are multiple items in it.", () => {
        let totalPrice = "44.00"
        cy.visit("/books")

        cy.addProduct(0)
        cy.addProduct(2)
        cy.addProduct(4)
        cy.get('.cart-qty').click()

        cy.get(':nth-child(1) > .cart-total-right > .nobr > .product-price')
            .should("have.text", totalPrice)

        //clearProducts done in hook afterEach tests to clean the state
    })

    it("Check invalid discount coupon code after adding 3 Items", () => {
        cy.visit("/books")

        cy.addProduct(0)
        cy.addProduct(2)
        cy.addProduct(4)
        cy.get('.cart-qty').click()
        cy.get('.coupon-box').find(".discount-coupon-code")
            .type("invalid code")
        cy.get('.coupon-box').find('.button-2')
            .click()

        cy.get('.coupon-box').find('.message')
            .should("contain", "couldn't be applied to your order")
    })

    it.only("Checkout products with valid coupon via Ground & Cash on Delivery", () => {
        cy.get('@data').then((data) => {
            const { firstname, lastname, email, country, city, address, postalCode, phoneNumber } = data.users[0]
            const { ground } = data.shippingMethod
            const { cash } = data.paymentMethod
            cy.visit("/books")

            cy.addProduct(0)
            cy.addProduct(2)
            cy.addProduct(4)
            cy.get('.cart-qty').click()

            cy.addCoupon("AutomationDiscount2")
            cy.get('#termsofservice').check()
            cy.get('#checkout').click()

            cy.addBillingAddress(firstname, lastname, email, country, city, address, postalCode, phoneNumber)
            cy.addShippingAddress(false) // false for no new address or pickup store
            cy.addShippingMethod(ground)
            cy.addPaymentMethod(cash)
            cy.checkPaymentInfo(cash)

            cy.get('.billing-info > .payment-method')
                .should('contain', "Cash On Delivery")

            cy.get('.shipping-info > .shipping-method')
                .should('contain', "Ground")

            cy.get('#confirm-order-buttons-container > .button-1').click()
            cy.get('.title > strong')
                .should("contain", "successfully")

            cy.clearAddresses()
        })
    })

    afterEach(() => {
        cy.clearProducts()
    })

    after(() => {
        cy.logout()
    })
})

function extractNumberFromShoppingCart(text) {
    const value = text.replace('(', "").replace(')', "")
    return parseInt(value);
}