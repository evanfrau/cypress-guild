///<reference types="Cypress"/>

describe('Account settings', () => {

    beforeEach(() => {
        cy.visit("/")
        cy.login(true)
        cy.fixture('testdata.json').as('data')
    })

    it("Add an adress to your account", () => {
        cy.get('@data').then((data) => {
            const { firstname,
                lastname,
                email,
                email2,
                addressCity,
                address1,
                postalCode,
                phoneNumber } = data.user
            const { titleAddress } = data.titles

            cy.get(".header-links > ul > :nth-child(1) > .account")
                .should("have.text", email).click()

            cy.get(":nth-child(2) > .inactive").click()

            cy.get(".add-button > .button-1").click()

            cy.get("h1")
                .should("have.text", titleAddress)

            cy.get("#Address_FirstName")
                .type(firstname)
                .should("have.value", firstname)

            cy.get("#Address_LastName")
                .type(lastname)
                .should("have.value", lastname)

            cy.get("#Address_Email")
                .type(email2)
                .should("have.value", email2)

            cy.get("#Address_CountryId").select('Belgium')

            cy.get("#Address_City")
                .type(addressCity)
                .should("have.value", addressCity)

            cy.get("#Address_Address1")
                .type(address1)
                .should("have.value", address1)

            cy.get("#Address_ZipPostalCode")
                .type(postalCode)
                .should("have.value", postalCode)

            cy.get("#Address_PhoneNumber")
                .type(phoneNumber)
                .should("have.value", phoneNumber)

            cy.get(".buttons > .button-1").click()

            cy.get(".address-list > :nth-child(1)").find(".title > strong")
                .should("have.text", `${firstname} ${lastname}`)
        })
    })

    it.only("Change password and log in with new password", () => {

        const password = Cypress.env('password');
        const newPassword = Cypress.env('newPassword');

        cy.checkEnvPassword(password);
        cy.checkEnvPassword(newPassword);

        cy.get('@data').then((data) => {
            const { email } = data.users[0]
            const { titleChangePass } = data.titles

            cy.get(".header-links > ul > :nth-child(1) > .account")
                .should("have.text", email).click()

            cy.get(":nth-child(7) > .inactive").click()

            cy.get("h1")
                .should("have.text", titleChangePass)

            cy.get("#OldPassword")
                .type(password)
                .should("have.value", password)

            cy.get("#NewPassword")
                .type(newPassword)
                .should("have.value", newPassword)

            cy.get("#ConfirmNewPassword")
                .type(newPassword)
                .should("have.value", newPassword)

            cy.get(".buttons > .button-1").click()

            cy.get(".result")
                .should("contain", "Password was changed")

            cy.get(".ico-logout").click()

            cy.login(true, email, newPassword)

            cy.get(".header-links > ul > :nth-child(1) > .account")
                .should("be.visible")
        })
    })

    it("Change personal details", () => {
        cy.get('@data').then((data) => {
            const { newFirstname, newLastname } = data.user
            const { titleChangeDetails } = data.titles

            cy.get(".header-links > ul > :nth-child(1) > .account")
                .click()

            cy.get("h1")
                .should("have.text", titleChangeDetails)

            cy.get("#gender-female").click()
                .should("be.checked")

            cy.get("#FirstName")
                .clear()
                .type(newFirstname)
                .should("have.value", newFirstname)

            cy.get("#LastName")
                .clear()
                .type(newLastname)
                .should("have.value", newLastname)

            cy.get(".buttons > .button-1").click()
        })
    })

    it("Cannot leave mandatory fields open", () => {
        cy.get('@data').then((data) => {
            const { titleChangeDetails } = data.titles
            const { requiredFirstname } = data.errors

            cy.get(".header-links > ul > :nth-child(1) > .account")
                .click()

            cy.get("h1")
                .should("have.text", titleChangeDetails)

            cy.get("#FirstName")
                .clear()

            cy.get(".buttons > .button-1").click()

            cy.get(".field-validation-error > span")
                .should("have.text", requiredFirstname)

        })
    })

    afterEach(() => {
        cy.clearAddresses()
        cy.changePassword()
        cy.logout()
    })
})