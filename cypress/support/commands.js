// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

Cypress.Commands.add("checkEnvPassword", (password
  ) => {
    if (!password) {
      throw new Error('Missing password value, set using CYPRESS_password=...')
    }
  });

Cypress.Commands.add("login", (isRememberChecked, email = "cypress@gmail.com", password = Cypress.env('password')) => {

  cy.checkEnvPassword(password);

  cy.get(".ico-login").click()
    .get(".returning-wrapper > .title > strong")
    .get("#Email").type(email)
    .get("#Password").type(password, {log: false})

  if (isRememberChecked) {
    cy.get('#RememberMe').check()
  }

  cy.get("form > .buttons > .button-1").click()
})

Cypress.Commands.add("logout", () => {
  cy.get('.ico-logout').click()
})

Cypress.Commands.add("changePassword", () => {
  const email = "cypress@gmail.com"
  const title2 = "My account - Change password"
  const oldPassword = Cypress.env('password')
  const newPassword = Cypress.env('newPassword')

  cy.checkEnvPassword(newPassword);

  cy.get(".header-links > ul > :nth-child(1) > .account")
    .should("have.text", email).click()

  cy.get(":nth-child(7) > .inactive").click()

  cy.get("h1")
    .should("have.text", title2)

  cy.get("#OldPassword")
    .type(oldPassword)
    .should("have.value", oldPassword)

  cy.get("#NewPassword")
    .type(newPassword)
    .should("have.value", newPassword)

  cy.get("#ConfirmNewPassword")
    .type(newPassword)
    .should("have.value", newPassword)

  cy.get(".buttons > .button-1").click()
})

Cypress.Commands.add("clearAddresses", () => {
  const email = "cypress@gmail.com"
  cy.get(".header-links > ul > :nth-child(1) > .account")
    .should("have.text", email).click()

  cy.get(":nth-child(2) > .inactive").click()

  cy.get('.center-2').then($body => {
    if ($body.find(".address-list >").length > 0) {
      cy.get('.address-list >').its('length').then(($el) => {
        cy.log("number of addresses " + $el)
        for (var k = 1; k <= $el; ++k) {
          cy.get(`.address-list > :nth-child(${k})`)
            .find('.buttons > .delete-address-button')
            .click()
        }
      })
    }
  })
})

Cypress.Commands.add("addProduct", (indexProduct) => {
  cy.intercept('POST', '/addproducttocart').as('addProduct')
  cy.get(".product-grid").children().eq(indexProduct).find(".button-2").click()
  cy.wait("@addProduct")
  cy.get('#bar-notification').should("be.visible")
})

Cypress.Commands.add("clearProducts", () => {
  cy.visit("http://demowebshop.tricentis.com/cart")
  cy.intercept('POST').as('removeProducts')
  //if this element has items in his shopping list
  cy.get('.center-1').then($body => {
    if ($body.find(".cart-item-row").length > 0) {
      cy.get('.cart-item-row').its('length').then(($el) => {
        cy.log("number of products " + $el)
        for (var k = 0; k < $el; ++k) {
          cy.get('.cart-item-row')
            .eq(k)
            .find('.remove-from-cart > input')
            .check()
        }
        if ($body.find('.current-code > .remove-discount-button').length > 0) {
          cy.get('.current-code > .remove-discount-button').click()
        }
        cy.get('.update-cart-button').click()
        cy.get('.order-summary-content').should("contain", "empty")
      })
    }
  });
})

Cypress.Commands.add("addCoupon", () => {
  cy.get('.coupon-box').find(".discount-coupon-code")
    .type("AutomationDiscount2")
  cy.get('.coupon-box').find('.button-2').click()

  cy.get('.coupon-box').find('.current-code')
    .should("be.visible")
    .should("contain", "AutomationDiscount2")
})

Cypress.Commands.add("addBillingAddress", (firstname, lastname, email, country, city, address, postalCode, phoneNumber) => {
  cy.get('#checkout-step-billing').then($body => {
    if ($body.find('#billing-address-select').length > 0) {
      cy.get('#billing-address-select').select("New Address")
    }
    cy.get('#BillingNewAddress_FirstName').clear().type(firstname)
    cy.get('#BillingNewAddress_LastName').clear().type(lastname)
    cy.get('#BillingNewAddress_Email').clear().type(email)
    cy.get('#BillingNewAddress_CountryId').select(country)
    cy.get('#BillingNewAddress_City').type(city)
    cy.get('#BillingNewAddress_Address1').type(address)
    cy.get('#BillingNewAddress_ZipPostalCode').type(postalCode)
    cy.get('#BillingNewAddress_PhoneNumber').type(phoneNumber)

    cy.get('#billing-buttons-container > .button-1').click()
  })
})

Cypress.Commands.add("addShippingAddress", (inStorePickupSelected) => {
  if (inStorePickupSelected)
    cy.get('#PickUpInStore').check()

  cy.get('#shipping-buttons-container > .button-1').click()
})

Cypress.Commands.add("addShippingMethod", (option) => {
  switch (option) {
    case 'GROUND':
      cy.get('#shippingoption_0').should("be.checked")
      break
    case 'NEXT_DAY_AIR':
      cy.get('#shippingoption_1').check()
      break
    case 'SECOND_DAY_AIR':
      cy.get('#shippingoption_2').check()
      break
    default:
      cy.get('#shippingoption_0').check()
      break
  }
  cy.get('#shipping-method-buttons-container > .button-1').click()
})

Cypress.Commands.add("addPaymentMethod", (option) => {
  switch (option) {
    case 'CASH':
      cy.get('#paymentmethod_0').should("be.checked")
      break
    case 'CHECK':
      cy.get('#paymentmethod_1').check()
      break
    case 'CREDIT_CARD':
      cy.get('#paymentmethod_2').check()
      break
    case 'PURCHASE_ORDER':
      cy.get('#paymentmethod_3').check()
      break
    default:
      cy.get('#paymentmethod_0').check()
      break
  }
  cy.get('#payment-method-buttons-container > .button-1').click()
})

Cypress.Commands.add("checkPaymentInfo", (option) => {
  let expectedText;
  switch (option) {
    case 'CASH':
      expectedText = "COD"
      break
    case 'CHECK':
      expectedText = "CHECK"
      break
    case 'CREDIT_CARD':
      expectedText = "credit card"
      break
    case 'PURCHASE_ORDER':
      expectedText = "PO"
      break
    default:
      throw new Error("type not found")
  }
  cy.get('#payment-info-buttons-container > .button-1').click()
})

const softAssertation = require("soft-assert")

Cypress.Commands.add('softAssert', (actual, expected, message) => {
  softAssertation.softAssert(actual, expected, message)
  cy.log(message)
});

Cypress.Commands.add('softAssertAll', () => softAssertation.softAssertAll());
