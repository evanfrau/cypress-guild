# Cypress Guild

This is a project for the Automation Battle Guild

It will use [Cypress](https://cypress.io)

## What is Cypress

Cypress is a testing tool written in JavaScript.
Used for Frontend E2E testing. API testing is also possible.

- Free
- Open Source
- No selenium
- Visual interface (tests with commands on the left and App on the right)
- DOM Snapshot history
- Use the JavaScript library [Mocha](https://mochajs.org/) to write tests
- Use the assertation library [Chai](https://www.chaijs.com/)
- Both libraries embrace BDD and TDD assertion styles

[Link to Cypress documentation](https://docs.cypress.io/guides/core-concepts/introduction-to-cypress#Cypress-is-Like-jQuery)

## Rules of the Automation Battle Guild

Automate using your favorite tool (here Cypress) the following website: [demowebshop by Tricentis](http://demowebshop.tricentis.com/)

__Session__ 
- February 03/2021 : Presentation & Define webiste
- February 25/2021 : Example Mapping
- March 23/2021 : Update & QA
- April 20/2021 : Presentation day
- June 22/2021 : Changes request

## Scenarios & Criteria links
_Goals: check all the boxes_
- [Scenarios to automate](https://gitlab.com/evanfrau/cypress-guild/-/issues/2)
- [Evaluation criteria](https://gitlab.com/evanfrau/cypress-guild/-/issues/4)
- [Change requests](https://gitlab.com/evanfrau/cypress-guild/-/issues/15)

_To the judges : please uncheck boxes that are not fullfilling the purpose_

## Our goal/vision 
Helping anyone that wants to self learn Cypress by following the lessons

## Lessons summary
- [Lesson 1](https://gitlab.com/evanfrau/cypress-guild/-/tree/lesson-1): How to install Cypress
- [Lesson 2](https://gitlab.com/evanfrau/cypress-guild/-/tree/lesson-2): How to run Cypress
- [Lesson 3](https://gitlab.com/evanfrau/cypress-guild/-/tree/lesson-3): How to run Cypress using the command terminal
- [Lesson 4](https://gitlab.com/evanfrau/cypress-guild/-/tree/lesson-4): Organize and Write a first test case
- [Lesson 5](https://gitlab.com/evanfrau/cypress-guild/-/tree/lesson-5): Refactor the test
- [Lesson 6](https://gitlab.com/evanfrau/cypress-guild/-/tree/lesson-6): How to use Cypress build-in reporting
- [Lesson 7](https://gitlab.com/evanfrau/cypress-guild/-/tree/lesson-7): How to add an HTML report
- [Lesson 8](https://gitlab.com/evanfrau/cypress-guild/-/tree/lesson-8): More advanced Cypress methods
- [Lesson 9](https://gitlab.com/evanfrau/cypress-guild/-/tree/lesson-9): Run the tests via Gitlab CI
- [Lesson 10](https://gitlab.com/evanfrau/cypress-guild/-/wikis/How-to-mask-sensitive-data): How to mask sensitive data

### Optional
- Lesson 11: Integrate a plugin (eg: Applitools)
- Lesson 12: Add a bot in Teams

### Team rules
- Following MOB pairing (Driver/Navigator)
- 1 session per week (2h)
- Split testcases after lessons 6
- Use Kanban [boards](https://gitlab.com/evanfrau/cypress-guild/-/boards) for follow up
- Comment code on ```WHY```
- **Agree to help anyone in the future**

## If you have any issue or request

You can create a ticket here: [Link to board](https://gitlab.com/evanfrau/cypress-guild/-/boards)

_Click on the + button in the column OPEN_


## IDE used
[Visual Studio Code](https://code.visualstudio.com/)

## Install the project from cloning this project

Clone the project...

```bash
git clone 
```

Install the dependencies...

```bash
npm install 
```

...then run Cypress

```bash
npm run cy:open 
```
## Or you can follow the lessons

Start the first lesson: [Install Cypress from scratch](https://gitlab.com/evanfrau/cypress-guild/-/tree/lesson-1)

